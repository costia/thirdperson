﻿using UnityEngine;
using System.Collections;

public class StartupScript : MonoBehaviour {
	
	public GameObject mPrefabChaser;
	public int mCount;
	GameObject[] mChasers;
	// Use this for initialization
	void Start () {
		mChasers=new GameObject[mCount];
		for (int i=0;i<mCount;i++){
			Vector3 pos=new Vector3(Random.Range(-40,+40),2,Random.Range(-40,+40));
			mChasers[i]=(GameObject)Instantiate(mPrefabChaser,pos,Quaternion.identity);				
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
