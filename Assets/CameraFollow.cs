﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	GameObject mPlayer;
	// Use this for initialization
	void Start () {
		mPlayer= GameObject.Find("Player");
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position=(mPlayer.transform.position+new Vector3(20,10,-20));
		this.transform.LookAt(mPlayer.transform.position);
	
	}
}
