﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
	CharacterController mController;
	Vector3 mFwd,mRight;
	
	// Use this for initialization
	void Start () {
		mFwd=Vector3.forward-Vector3.right;
		mFwd.Normalize();
		mRight=Vector3.forward+Vector3.right;
		mRight.Normalize();
		mController=GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.W)){
			mController.SimpleMove(5*mFwd);
		}
		if (Input.GetKey(KeyCode.S)){
			mController.SimpleMove(-5*mFwd);
		}
		
		if (Input.GetKey(KeyCode.D)){
			mController.SimpleMove(5*mRight);
		}
		if (Input.GetKey(KeyCode.A)){
			mController.SimpleMove(-5*mRight);
		}
		
	}
}
